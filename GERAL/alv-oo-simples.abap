*----------------------------------------------------------------------*
*----------------------------------------------------------------------*
* Projeto .......: [WM] Relatório de tempo de armazenagem.             *
* Objetivo ......: Listar materiais e tempo de armazenagem dos mesmos. *
* Solicitante ...: Leandro Gonçalves                                   *
* Autor .........: Carlos Augusto                                      *
* Data desenv ...: 04/01/2016                                          *
*----------------------------------------------------------------------*
REPORT  zwmr174.

*----------------------------------------------------------------------*
*  Includes
*----------------------------------------------------------------------*
INCLUDE zwmr174_top.   "Include com as declarações de types e dados
INCLUDE zwmr174_f01.   "Include com as rotinas ABAP
INCLUDE zwmr174_o01.   "Include before output
INCLUDE zwmr174_i01.   "Include after input

*----------------------------------------------------------------------*
*  Processo Principal
*----------------------------------------------------------------------*
START-OF-SELECTION.

  "Válida dados da tela de seleção...
  PERFORM f_valida_tela_selecao.

  "Válida autorizações...
  PERFORM f_valida_autorizacao.

  "Limpa os dados das tabelas internas
  PERFORM f_inicializa_tabelas.

  "Seleciona dados
  PERFORM f_seleciona_dados.

  "Processa dados...
  PERFORM f_processa_dados.

  "Chama tela - Relatório ALV...
  CALL SCREEN 0200.
  
  
*&---------------------------------------------------------------------*
*&  Include           ZWMR174_TOP
*&---------------------------------------------------------------------*
*----------------------------------------------------------------------*
*  Type pools
*----------------------------------------------------------------------*
TYPE-POOLS: abap,
            icon.

*----------------------------------------------------------------------*
*  Tables
*----------------------------------------------------------------------*
TABLES: mara,  "Dados de material por sistema de depósito
        mlgn,  "Dados de material por sistema de depósito
        ltak,  "Cabeçalho de ordem de transporte SAD
        ltap.  "Item de ordem de transporte

*----------------------------------------------------------------------*
*  Types
*----------------------------------------------------------------------*
TYPES:
  BEGIN OF ty_lqua,
    lgnum TYPE lqua-lgnum,
    matnr TYPE lqua-matnr,
    werks TYPE lqua-werks,
    charg TYPE lqua-charg,
    lgtyp TYPE lqua-lgtyp,
    lgpla TYPE lqua-lgpla,
    btanr TYPE lqua-btanr,
    lenum TYPE lqua-lenum,
    meins TYPE lqua-meins,
    verme TYPE lqua-verme,
    gesme TYPE lqua-gesme,
    vfdat TYPE lqua-vfdat,
    lgort TYPE lqua-lgort,
    mblnr TYPE mkpf-mblnr,
    END OF ty_lqua,

  BEGIN OF ty_t334t,
    lgnum TYPE t334t-lgnum,
    lgtkz TYPE t334t-lgtkz,
    lgty0 TYPE t334t-lgty0,
    END OF ty_t334t,

  BEGIN OF ty_ltap,
    lgnum TYPE ltap-lgnum,
    tanum TYPE ltap-tanum,
    matnr TYPE ltap-matnr,
    werks TYPE ltap-werks,
    lgort TYPE ltap-lgort,
    charg TYPE ltap-charg,
    meins TYPE ltap-meins,
    qdatu TYPE ltap-qdatu,
    qzeit TYPE ltap-qzeit,
    vltyp TYPE ltap-vltyp,
    vlpla TYPE ltap-vlpla,
    nltyp TYPE ltap-nltyp,
    nlpla TYPE ltap-nlpla,
    nistm TYPE ltap-nistm,
    nlenr TYPE ltap-nlenr,
    vfdat TYPE ltap-vfdat,
    bdatu TYPE ltak-bdatu,
    bzeit TYPE ltak-bzeit,
    pquit TYPE ltap-pquit,
    pvqui TYPE ltap-pvqui,
    vorga TYPE ltap-vorga,
    END OF ty_ltap,

  BEGIN OF ty_makt,
    matnr TYPE makt-matnr,
    maktx TYPE makt-maktx,
    END OF ty_makt,

  BEGIN OF ty_zwmt143,
    lgnum    TYPE zwmt143-lgnum,
    werks    TYPE zwmt143-werks,
    lgort    TYPE zwmt143-lgort,
    ltkze    TYPE t334t-lgtkz,
    timearm1 TYPE zwmt143-timearm1,
    timearm2 TYPE zwmt143-timearm2,
    timelimi TYPE zwmt143-timelimi,
    END OF ty_zwmt143,

  BEGIN OF ty_zwmt145,
    lgnum     TYPE zwmt145-lgnum,
    werks     TYPE zwmt145-werks,
    lgort     TYPE zwmt145-lgort,
    lgtyp_de  TYPE zwmt145-lgtyp_de,
    lgtyp_ate TYPE zwmt145-lgtyp_ate,
    END OF ty_zwmt145,

  BEGIN OF ty_sum,
    lgnum      TYPE lqua-lgnum,
    werks      TYPE lqua-werks,
    lgort      TYPE lqua-lgort,
    matnr      TYPE lqua-matnr,
    maktx      TYPE makt-maktx,
    charg      TYPE lqua-charg,
    gesme      TYPE lqua-gesme,
    meins      TYPE lqua-meins,
    lgtyp      TYPE lqua-lgtyp,
    lgpla      TYPE lqua-lgpla,
    timearm1   TYPE zwmt143-timearm1,
    timearm1_t TYPE zwmt143-timearm1,
    timearm2   TYPE zwmt143-timearm2,
    timearm2_t TYPE zwmt143-timearm2,
    atividade  TYPE zwmt100-atividade,
    END OF ty_sum,

  BEGIN OF ty_zwmt100,
    werks      TYPE zwmt100-werks,
    lgort      TYPE zwmt100-lgort,
    lgnum      TYPE zwmt100-lgnum,
    chave_ativ TYPE zwmt100-chave_ativ,
    atividade  TYPE zwmt100-atividade,
    END OF ty_zwmt100,

  BEGIN OF ty_zwmt102,
    tp_reg      TYPE zwmt102-tp_reg,
    chave_reg   TYPE zwmt102-chave_reg,
    chave_ativ  TYPE zwmt102-chave_ativ,
    status_erro TYPE zwmt102-status_erro,
    status_ok   TYPE zwmt102-status_ok,
    status_all  TYPE zwmt102-status_all,
    chave_grp   TYPE zwmt101-chave_grp,
    END OF ty_zwmt102,

  BEGIN OF ty_zwmt101,
    uname     TYPE zwmt101-uname,
    chave_grp TYPE zwmt101-chave_grp,
    END OF ty_zwmt101,

  BEGIN OF ty_email,
    email      TYPE ad_smtpadr,
    atividade  TYPE zwmt100-atividade,
    END OF ty_email,

  BEGIN OF ty_mensagem,
    line  TYPE so_text255,
    END OF ty_mensagem,

  BEGIN OF ty_mkpf,
    mblnr TYPE mkpf-mblnr,
    budat TYPE mkpf-budat,
    cputm TYPE mkpf-cputm,
    END OF ty_mkpf,

 BEGIN OF ty_zwmt059,
   lgnum     TYPE zwmt059-lgnum,
   betyp     TYPE zwmt059-betyp,
   docproc   TYPE zwmt059-docproc,
   lenum     TYPE zwmt059-lenum,
   volmast   TYPE zwmt059-volmast,
   nvers     TYPE zwmt059-nvers,
   matnr     TYPE zwmt059-matnr,
   lichn     TYPE zwmt059-lichn,
   item      TYPE zwmt059-item,
   charg     TYPE zwmt059-charg,
   verme     TYPE zwmt059-verme,
   meins     TYPE zwmt059-meins,
   dt_armaz  TYPE zwmt059-dt_armaz,
   hr_armaz  TYPE zwmt059-hr_armaz,
   erdat_59  TYPE zwmt059-erdat,
   erzet_59  TYPE zwmt059-erzet,
   vlpyp     TYPE zwmt056-vltyp,
   vlpla     TYPE zwmt056-vlpla,
   erdatterm TYPE zwmt056-erdatterm,
   erzetterm TYPE zwmt056-erzetterm,
   erdat     TYPE zwmt056-erdat,
   erzet     TYPE zwmt056-erzet,
   doca      TYPE zwmt056-doca,
   status    TYPE zwmt056-status,
   END OF ty_zwmt059,

 BEGIN OF ty_zwmt059_sum,
   lgnum     TYPE zwmt059-lgnum,
   betyp     TYPE zwmt059-betyp,
*   docproc   TYPE zwmt059-docproc,
*   lenum     TYPE zwmt059-lenum,
*   volmast   TYPE zwmt059-volmast,
*   nvers     TYPE zwmt059-nvers,
   matnr     TYPE zwmt059-matnr,
   lichn     TYPE zwmt059-lichn,
   item      TYPE zwmt059-item,
   charg     TYPE zwmt059-charg,
   verme     TYPE zwmt059-verme,
   meins     TYPE zwmt059-meins,
   dt_armaz  TYPE zwmt059-dt_armaz,
   hr_armaz  TYPE zwmt059-hr_armaz,
   erdat_59  TYPE zwmt059-erdat,
   erzet_59  TYPE zwmt059-erzet,
   erdat     TYPE zwmt056-erdat,
   erzet     TYPE zwmt056-erzet,
*   vlpyp     TYPE zwmt056-vltyp,
*   vlpla     TYPE zwmt056-vlpla,
   erdatterm TYPE zwmt056-erdatterm,
   erzetterm TYPE zwmt056-erzetterm,
   doca      TYPE zwmt056-doca,
   status    TYPE zwmt056-status,
   END OF ty_zwmt059_sum,

 BEGIN OF ty_t30c,
   lgnum TYPE t30c-lgnum,
   lgbzo TYPE t30c-lgbzo,
   lgtor TYPE t30c-lgtor,
   END OF ty_t30c,

 BEGIN OF ty_mlgn,
   lgnum TYPE mlgn-lgnum,
   ltkze TYPE mlgn-ltkze,
   matnr TYPE mlgn-matnr,
   lgty0 TYPE t334t-lgty0,
   END OF ty_mlgn,

 BEGIN OF ty_saida.
        INCLUDE STRUCTURE zwms296.
TYPES:
   cellcolor  TYPE lvc_t_scol,
  END OF ty_saida,

 BEGIN OF ty_saida_2,
   lgnum      TYPE zwms296-lgnum,
   werks      TYPE zwms296-werks,
   lgort      TYPE zwms296-lgort,
   matnr      TYPE zwms296-matnr,
   charg      TYPE zwms296-charg,
   maktx      TYPE zwms296-maktx,
   nistm      TYPE zwms296-nistm,
   meins      TYPE zwms296-meins,
   qdatu      TYPE zwms296-qdatu,
   qzeit      TYPE zwms296-qzeit,
   lgtyp      TYPE zwms296-lgtyp,
   ltypt      TYPE t301t-ltypt,
   lgpla      TYPE zwms296-lgpla,
   bdatu      TYPE zwms296-bdatu,
   bzeit      TYPE zwms296-bzeit,
   time_1     TYPE char30,
   time_2     TYPE char30,
   time_r     TYPE char30,
   icon_time1 TYPE zwms296-icon_time1,
   icon_time2 TYPE zwms296-icon_time2,
   icon_tran  TYPE zwms296-icon_tran,
   umt        TYPE char3,
   ltkze      TYPE mlgn-ltkze,
   status_56  TYPE zwmt056-status,
   ativid_56  TYPE xfeld,
   tanum      TYPE ltak-tanum,
   data_ini   TYPE ltap-qdatu,
   hora_ini   TYPE ltap-qzeit,
   timelimi   TYPE zwms296-timelimi,
   END OF ty_saida_2,

 BEGIN OF ty_t301t,
  lgnum      TYPE t301t-lgnum,
  lgtyp      TYPE t301t-lgtyp,
  ltypt      TYPE t301t-ltypt,
  END OF ty_t301t.

*----------------------------------------------------------------------*
*  Variáveis/Tables/Estruturas
*----------------------------------------------------------------------*
DATA: gt_fieldcat  TYPE lvc_t_fcat,
      gs_fieldcat  TYPE lvc_s_fcat,
      "
      gt_sort      TYPE lvc_t_sort,
      gs_sort      TYPE lvc_s_sort,
      "
      gt_ui_func   TYPE ui_functions,
      "
      gs_layout    TYPE lvc_s_layo,
      "
      gs_variant   TYPE disvariant,
      "
      gt_saida     TYPE TABLE OF ty_saida_2,
      gs_saida     TYPE ty_saida_2,
      "
      gv_ucomm     TYPE sy-ucomm,
      "
      gv_transito  TYPE zde_icon,
      "
      gt_lqua              TYPE TABLE OF ty_lqua,
      gs_lqua              TYPE ty_lqua,
      "
      gt_t334t             TYPE TABLE OF ty_t334t,
      gs_t334t             TYPE ty_t334t,
      "
      gt_ltap              TYPE TABLE OF ty_ltap,
      gs_ltap              TYPE ty_ltap,
      "
      gs_makt              TYPE ty_makt,
      gt_makt              TYPE TABLE OF ty_makt,
      "
      gt_ltap_aux          TYPE TABLE OF ty_ltap,
      gs_ltap_aux          TYPE ty_ltap,
      "
      gs_ltap_before       TYPE ty_ltap,
      "
      gt_zwmt143           TYPE TABLE OF ty_zwmt143,
      gs_zwmt143           TYPE ty_zwmt143,
      "
      gt_sum               TYPE TABLE OF ty_sum,
      gs_sum               TYPE ty_sum,
      "
      gt_mkpf              TYPE TABLE OF ty_mkpf,
      gs_mkpf              TYPE ty_mkpf,
      "
      gt_zwmt059           TYPE TABLE OF ty_zwmt059,
      gs_zwmt059           TYPE ty_zwmt059,
      "
      gt_zwmt059_sum       TYPE TABLE OF ty_zwmt059_sum,
      gs_zwmt059_sum       TYPE ty_zwmt059_sum,
      "
      gt_t30c              TYPE TABLE OF ty_t30c,
      gs_t30c              TYPE ty_t30c,
      "
      gt_mlgn              TYPE TABLE OF ty_mlgn,
      gs_mlgn              TYPE ty_mlgn,
      "
      gt_zwmt145           TYPE TABLE OF ty_zwmt145,
      gs_zwmt145           TYPE ty_zwmt145,
      "
      gt_t301t      TYPE TABLE OF ty_t301t,
      gs_t301t       TYPE ty_t301t,
      "
      gr_status_fim_conf   TYPE RANGE OF zwmt059-status WITH HEADER LINE.

*----------------------------------------------------------------------*
*  Classes
*----------------------------------------------------------------------*
DATA: cl_custom_container TYPE REF TO cl_gui_custom_container,
      cl_alv_grid         TYPE REF TO cl_gui_alv_grid.

*----------------------------------------------------------------------*
*  Constantes
*----------------------------------------------------------------------*
CONSTANTS: c_e TYPE t334t-kzear                   VALUE 'E',
           c_betyp_c       TYPE zwmt056-betyp     VALUE 'C',
           c_conf          TYPE zwmt056-operacao  VALUE 'CONF',
           c_f             TYPE zwmt056-status    VALUE 'F'.
*----------------------------------------------------------------------*
*  Opções de Seleção
*----------------------------------------------------------------------*
"Tela de Seleção
SELECTION-SCREEN BEGIN OF BLOCK b1 WITH FRAME TITLE text-001.
PARAMETERS:
    p_lgnum       TYPE lgnum   OBLIGATORY,    "Nºdepósito/complexo de depósito...
    p_werks       TYPE werks_d OBLIGATORY,    "Centro...
    p_lgort       TYPE lgort_d OBLIGATORY.    "Depósito...
SELECTION-SCREEN END   OF BLOCK b1.
SELECTION-SCREEN BEGIN OF BLOCK b2 WITH FRAME TITLE text-002.
SELECT-OPTIONS:
*    s_ltkze       FOR mlgn-ltkze OBLIGATORY, "Código da categoria de depósito para entrada em depósito
    s_matnr       FOR ltap-matnr,            "Material...
    s_charg       FOR ltap-charg,            "Lote...
    s_bdatu       FOR ltak-bdatu OBLIGATORY. "Data de criação da ordem de transferência
SELECTION-SCREEN END   OF BLOCK b2.

*&---------------------------------------------------------------------*
*&  Include           ZWMR174_F01
*&---------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Form  F_MONTA_FIELDCAT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_monta_fieldcat .
  "Monta colunas de saida do relatório com o formato da estrutura
  "criada para o mesmo
  DATA: vl_struct_name TYPE dd02l-tabname.
  FIELD-SYMBOLS: <fs_fieldcat> TYPE lvc_s_fcat.

  "Estrutura para reimpressão de etiquetas...
  vl_struct_name = 'ZWMS296'.

  "Busca os campos da estrutura para carregar o fieldcat do alv
  CALL FUNCTION 'LVC_FIELDCATALOG_MERGE'
    EXPORTING
      i_structure_name       = vl_struct_name
      i_bypassing_buffer     = abap_true
    CHANGING
      ct_fieldcat            = gt_fieldcat
    EXCEPTIONS
      inconsistent_interface = 1
      program_error          = 2
      OTHERS                 = 3.

  LOOP AT gt_fieldcat ASSIGNING <fs_fieldcat>.
    <fs_fieldcat>-col_opt   = abap_true.
    IF <fs_fieldcat>-fieldname EQ 'ICON_TRAN'.
      <fs_fieldcat>-scrtext_l = 'Trânsito'.
      <fs_fieldcat>-scrtext_m = 'Trânsito'.
      <fs_fieldcat>-scrtext_s = 'Trânsito'.
      <fs_fieldcat>-reptext   = 'Trânsito'.
    ELSEIF ( <fs_fieldcat>-fieldname EQ 'ICON_TIME1' )
      OR ( <fs_fieldcat>-fieldname EQ 'TIME_1' ).
      <fs_fieldcat>-scrtext_l = 'Tempo 1'.
      <fs_fieldcat>-scrtext_m = 'Tempo 1'.
      <fs_fieldcat>-scrtext_s = 'Tempo 1'.
      <fs_fieldcat>-reptext   = 'Tempo 1'.
      CHECK <fs_fieldcat>-fieldname EQ 'TIME_1'.
      <fs_fieldcat>-no_out = abap_true.
    ELSEIF ( <fs_fieldcat>-fieldname EQ 'ICON_TIME2' )
      OR ( <fs_fieldcat>-fieldname EQ 'TIME_2' ).
      <fs_fieldcat>-scrtext_l = 'Tempo 2'.
      <fs_fieldcat>-scrtext_m = 'Tempo 2'.
      <fs_fieldcat>-scrtext_s = 'Tempo 2'.
      <fs_fieldcat>-reptext   = 'Tempo 2'.
      CHECK <fs_fieldcat>-fieldname EQ 'TIME_2'.
      <fs_fieldcat>-no_out = abap_true.
    ELSEIF <fs_fieldcat>-fieldname EQ 'NISTM'.
      <fs_fieldcat>-scrtext_l = 'Qtd.'.
      <fs_fieldcat>-scrtext_m = 'Qtd.'.
      <fs_fieldcat>-scrtext_s = 'Qtd.'.
      <fs_fieldcat>-reptext   = 'Qtd.'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'QDATU'.
      <fs_fieldcat>-scrtext_l = 'Dt. Ent. Dep.'.
      <fs_fieldcat>-scrtext_m = 'Dt. Ent. Dep.'.
      <fs_fieldcat>-scrtext_s = 'Dt. Ent. Dep.'.
      <fs_fieldcat>-reptext   = 'Dt. Ent. Dep.'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'QZEIT'.
      <fs_fieldcat>-scrtext_l = 'Hr. Ent. Dep.'.
      <fs_fieldcat>-scrtext_m = 'Hr. Ent. Dep.'.
      <fs_fieldcat>-scrtext_s = 'Hr. Ent. Dep.'.
      <fs_fieldcat>-reptext   = 'Hr. Ent. Dep.'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'BDATU'.
      <fs_fieldcat>-scrtext_l = 'Dt. Saída Dep.'.
      <fs_fieldcat>-scrtext_m = 'Dt. Saída Dep.'.
      <fs_fieldcat>-scrtext_s = 'Dt. Saída Dep.'.
      <fs_fieldcat>-reptext   = 'Dt. Saída Dep.'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'BZEIT'.
      <fs_fieldcat>-scrtext_l = 'Hr. Saída Dep.'.
      <fs_fieldcat>-scrtext_m = 'Hr. Saída Dep.'.
      <fs_fieldcat>-scrtext_s = 'Hr. Saída Dep.'.
      <fs_fieldcat>-reptext   = 'Hr. Saída Dep.'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'TIME_R'.
      <fs_fieldcat>-scrtext_l = 'Tempo Total'.
      <fs_fieldcat>-scrtext_m = 'Tempo Total'.
      <fs_fieldcat>-scrtext_s = 'Tempo Total'.
      <fs_fieldcat>-reptext   = 'Tempo Total'.
      <fs_fieldcat>-col_opt   = abap_false.
      <fs_fieldcat>-outputlen = 12.
    ELSEIF <fs_fieldcat>-fieldname EQ 'UMT'.
      <fs_fieldcat>-scrtext_l = 'UMT'.
      <fs_fieldcat>-scrtext_m = 'UMT'.
      <fs_fieldcat>-scrtext_s = 'UMT'.
      <fs_fieldcat>-reptext   = 'UMT'.
    ELSEIF <fs_fieldcat>-fieldname EQ 'TIMELIMI'.
      <fs_fieldcat>-scrtext_l = 'Tempo Limite'.
      <fs_fieldcat>-scrtext_m = 'Tempo Limite'.
      <fs_fieldcat>-scrtext_s = 'Tempo Limite'.
      <fs_fieldcat>-reptext   = 'Tempo Limite'.
      <fs_fieldcat>-col_opt   = abap_false.
      <fs_fieldcat>-outputlen = 12.
    ELSEIF <fs_fieldcat>-fieldname EQ 'LGPLA'.
      <fs_fieldcat>-col_opt   = abap_false.
      <fs_fieldcat>-outputlen = 12.
*    ELSEIF ( <fs_fieldcat>-fieldname EQ 'MATNR'
*      OR <fs_fieldcat>-fieldname EQ 'CHARG'
*        OR <fs_fieldcat>-fieldname EQ 'LGNUM'
*          OR <fs_fieldcat>-fieldname EQ 'WERKS'
*            OR <fs_fieldcat>-fieldname EQ 'LGORT' ).
*      break casilva03.
*      <fs_fieldcat>-fix_column = abap_true.
*      <fs_fieldcat>-key        = abap_true.
    ELSE.
      <fs_fieldcat>-col_opt   = abap_true.
    ENDIF.
  ENDLOOP.

ENDFORM.                    " F_MONTA_FIELDCAT
*&---------------------------------------------------------------------*
*&      Form  F_INICIALIZA_TABELAS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_inicializa_tabelas.
  "Limpa tabelas internas...
  REFRESH: gt_mlgn[], gt_t334t[],
           gt_ltap[], gt_saida[].

  gv_transito = icon_led_red.

ENDFORM.                    " F_INICIALIZA_TABELAS
*&---------------------------------------------------------------------*
*&      Form  F_SELECIONA_DADOS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_seleciona_dados.

  DATA: r_lgtyp TYPE RANGE OF lgtyp WITH HEADER LINE.

  FIELD-SYMBOLS: <fs_lqua>    TYPE ty_lqua,
                 <fs_zwmt143> TYPE ty_zwmt143.

  "Tempo de Armazenamento - Tipo de Depósito...
  REFRESH gt_zwmt143[].
  SELECT lgnum werks   lgort
         ltkze timearm1 timearm2 timelimi
    FROM zwmt143
    INTO TABLE gt_zwmt143
    WHERE lgnum EQ p_lgnum
      AND werks EQ p_werks
      AND lgort EQ p_lgort.

  IF sy-subrc IS NOT INITIAL.
    "Configurações de tempo não encontradas para seleção...
    MESSAGE s003(zwm) DISPLAY LIKE 'E'.
    STOP.
  ENDIF.

  CHECK gt_zwmt143[] IS NOT INITIAL.

  REFRESH gt_zwmt145[].
  SELECT lgnum    werks     lgort
         lgtyp_de lgtyp_ate
    FROM zwmt145
    INTO TABLE gt_zwmt145
    WHERE lgnum EQ p_lgnum
      AND werks EQ p_werks
      AND lgort EQ p_lgort.

  LOOP AT gt_zwmt145 INTO gs_zwmt145.
    CLEAR r_lgtyp.
    r_lgtyp-sign = 'I'.
    IF gs_zwmt145-lgtyp_ate IS INITIAL.
      r_lgtyp-option = 'EQ'.
      r_lgtyp-low = gs_zwmt145-lgtyp_de.
    ELSE.
      r_lgtyp-option = 'BT'.
      r_lgtyp-low = gs_zwmt145-lgtyp_de.
      r_lgtyp-high = gs_zwmt145-lgtyp_ate.
    ENDIF.
    APPEND r_lgtyp.
  ENDLOOP.

  REFRESH gt_mlgn[].
  SELECT mlgn~lgnum mlgn~ltkze
         mlgn~matnr t334t~lgty0
    FROM mlgn
    INNER JOIN t334t ON
        mlgn~lgnum EQ t334t~lgnum
    AND mlgn~ltkze EQ t334t~lgtkz
    INTO TABLE gt_mlgn
    FOR ALL ENTRIES IN gt_zwmt143
    WHERE mlgn~matnr  IN s_matnr
      AND mlgn~lgnum  EQ p_lgnum
      AND mlgn~ltkze  EQ gt_zwmt143-ltkze
      AND t334t~kzear EQ c_e
      AND bwref       EQ space.

  IF sy-subrc IS NOT INITIAL.
    "Não existem dados para a seleção do usuário
    MESSAGE s003(zwm) DISPLAY LIKE 'E'.
    STOP.
  ENDIF.

  CHECK gt_mlgn[] IS NOT INITIAL.

  "Quantos...
  REFRESH gt_lqua[].
  SELECT lqua~lgnum lqua~matnr lqua~werks lqua~charg
         lqua~lgtyp lqua~lgpla lqua~btanr lqua~lenum
         lqua~meins lqua~verme lqua~gesme lqua~vfdat
         lqua~lgort
    FROM lqua
    INTO TABLE gt_lqua
    FOR ALL ENTRIES IN gt_mlgn
    WHERE lqua~matnr EQ gt_mlgn-matnr
      AND lqua~lgnum EQ gt_mlgn-lgnum
      AND lqua~lgtyp NE gt_mlgn-lgty0.

  "--"---"------------------------------------------------
  "Elimina registros que não pertencem ao Centro e Depósito informado...
  DELETE gt_lqua[] WHERE werks NE p_werks.
  DELETE gt_lqua[] WHERE lgort NE p_lgort.
  DELETE gt_lqua[] WHERE charg NOT IN s_charg.
  IF r_lgtyp[] IS NOT INITIAL.
    DELETE gt_lqua[] WHERE lgtyp IN r_lgtyp[].
  ENDIF.
  "--"---"------------------------------------------------

  "Remove materiais, que não possui estoque disponivel(Elinima materiais que so possui estoque entrando)...
  DELETE gt_lqua WHERE gesme <= 0.

  "Preenche campo auxliar doc. material, para seleção for all entries...
  LOOP AT gt_lqua ASSIGNING <fs_lqua>.
    <fs_lqua>-mblnr = <fs_lqua>-btanr.
  ENDLOOP.

  IF gt_lqua[] IS NOT INITIAL.
    "Cabeçalho do documento do material...
    REFRESH gt_mkpf[].
    SELECT mblnr budat cputm
      FROM mkpf
      INTO TABLE gt_mkpf
      FOR ALL ENTRIES IN gt_lqua
      WHERE budat IN s_bdatu
        AND mblnr EQ gt_lqua-mblnr.

    "Seleciona Denominação da ctg.depósito...
    REFRESH gt_t301t[].
    SELECT lgnum lgtyp ltypt
      FROM t301t
      INTO TABLE gt_t301t
      FOR ALL ENTRIES IN gt_lqua
      WHERE spras EQ sy-langu
        AND lgnum EQ gt_lqua-lgnum
        AND lgtyp EQ gt_lqua-lgtyp.
  ENDIF.

  "Item de ordem de transporte...
  REFRESH gt_ltap[].
  SELECT ltap~lgnum ltap~tanum ltap~matnr ltap~werks ltap~lgort
         ltap~charg ltap~meins ltap~qdatu ltap~qzeit
         ltap~vltyp ltap~vlpla ltap~nltyp ltap~nlpla
         ltap~nistm ltap~nlenr ltap~vfdat ltak~bdatu
         ltak~bzeit ltap~pquit ltap~pvqui ltap~vorga
    FROM ltap
    INNER JOIN ltak ON
        ltap~lgnum EQ ltak~lgnum
    AND ltap~tanum EQ ltak~tanum
    INTO TABLE gt_ltap
    FOR ALL ENTRIES IN gt_mlgn
    WHERE ltap~lgnum EQ gt_mlgn-lgnum
      AND ltap~pquit EQ abap_true
      AND ltap~matnr EQ gt_mlgn-matnr
      AND ltap~nltyp NE gt_mlgn-lgty0 "->Verificar necessidade de criar novo indice...
      AND ltap~charg IN s_charg. "->Verificar necessidade de criar novo indice...

  "--"---"------------------------------------------------
  "Elimina registros que não pertencem ao Centro e Depósito informado...
  DELETE gt_ltap[] WHERE qdatu NOT IN s_bdatu.
  DELETE gt_ltap[] WHERE werks NE p_werks.
  DELETE gt_ltap[] WHERE lgort NE p_lgort.
  IF r_lgtyp[] IS NOT INITIAL.
    DELETE gt_ltap[] WHERE nltyp IN r_lgtyp[].
  ENDIF.
  "--"---"------------------------------------------------

  IF gt_ltap[] IS NOT INITIAL.
    "Seleciona Denominação da ctg.depósito...
    SELECT lgnum lgtyp ltypt
      FROM t301t
      APPENDING TABLE gt_t301t
      FOR ALL ENTRIES IN gt_ltap
      WHERE spras EQ sy-langu
        AND lgnum EQ gt_ltap-lgnum
        AND lgtyp EQ gt_ltap-nltyp.
  ENDIF.

  "Seleciona materiais que estão em conferência...
  REFRESH gt_zwmt059[].
  SELECT zwmt059~lgnum     zwmt059~betyp    zwmt059~docproc
         zwmt059~lenum     zwmt059~volmast  zwmt059~nvers
         zwmt059~matnr     zwmt059~lichn    zwmt059~item
         zwmt059~charg     zwmt059~verme    zwmt059~meins
         zwmt059~dt_armaz  zwmt059~hr_armaz zwmt059~erdat
         zwmt059~erzet     zwmt056~vltyp    zwmt056~vlpla
         zwmt056~erdat     zwmt056~erzet    zwmt056~erdatterm
         zwmt056~erzetterm zwmt056~doca     zwmt056~status
    FROM zwmt056
    INNER JOIN zwmt059 ON
        zwmt056~lgnum   EQ zwmt059~lgnum
    AND zwmt056~betyp   EQ zwmt059~betyp
*    AND zwmt056~nvers   EQ zwmt059~nvers
    AND zwmt056~docproc EQ zwmt059~docproc
    INTO TABLE gt_zwmt059
    FOR ALL ENTRIES IN gt_mlgn
    WHERE   zwmt056~lgnum    EQ p_lgnum
      AND   zwmt056~betyp    EQ c_betyp_c
      AND   zwmt056~operacao EQ c_conf
      AND   zwmt059~nvers    EQ space
      AND   zwmt059~matnr    EQ gt_mlgn-matnr.

  "--"---"------------------------------------------------
  DELETE gt_zwmt059[] WHERE  ( charg NOT IN s_charg
                            AND lichn NOT IN s_charg ).
  DELETE gt_zwmt059[] WHERE erdatterm NOT IN s_bdatu.
  "--"---"------------------------------------------------

  IF gt_zwmt059[] IS NOT INITIAL.
    REFRESH gt_t30c[].
    SELECT lgnum lgbzo lgtor
      FROM t30c
      INTO TABLE gt_t30c
      FOR ALL ENTRIES IN gt_zwmt059
      WHERE lgnum EQ p_lgnum
        AND lgbzo EQ gt_zwmt059-doca.

    IF gt_t30c[] IS NOT INITIAL.
      "Seleciona Denominação da ctg.depósito...
      SELECT lgnum lgtyp ltypt
        FROM t301t
        APPENDING TABLE gt_t301t
        FOR ALL ENTRIES IN gt_t30c
        WHERE spras EQ sy-langu
          AND lgnum EQ gt_t30c-lgnum
          AND lgtyp EQ gt_t30c-lgtor.
    ENDIF.
  ENDIF.

  "Remove duplicados...
  SORT gt_t30c[].
  DELETE ADJACENT DUPLICATES FROM gt_t30c[] COMPARING ALL FIELDS.

  "Textos breves de material...
  REFRESH gt_makt[].
  SELECT matnr maktx
    FROM makt
    INTO TABLE gt_makt
    FOR ALL ENTRIES IN gt_mlgn
    WHERE matnr EQ gt_mlgn-matnr
      AND spras EQ sy-langu.

  "Elimina Itens estornados...
  DELETE gt_ltap[] WHERE (   pquit EQ 'X'    OR pvqui EQ 'X' )
                     AND ( ( vorga EQ 'ST' ) OR ( vorga EQ 'SL' ) ).

  IF ( gt_ltap[] IS INITIAL )
      AND ( gt_zwmt059[] IS INITIAL ).
    "Não existe materiais fora do local de armazenagem...
    MESSAGE s000(zwm) WITH text-m02 DISPLAY LIKE 'E'.
    STOP.
  ENDIF.

ENDFORM.                    " F_SELECIONA_DADOS
*&---------------------------------------------------------------------*
*&      Form  F_VALIDA_TELA_SELECAO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_valida_tela_selecao.
  "Verifica centro e deposito na tela de seleção...
  SELECT COUNT(*)
    FROM t320
    WHERE werks EQ p_werks
      AND lgort EQ p_lgort
      AND lgnum EQ p_lgnum.

  IF sy-subrc IS NOT INITIAL.
    "Sistema de Depósito & inválido para Centro &, Depósito &.
    MESSAGE s061(zwm) WITH p_lgnum p_werks p_lgort DISPLAY LIKE 'E'.
    LEAVE LIST-PROCESSING.
  ENDIF.

ENDFORM.                    " F_VALIDA_TELA_SELECAO
*&---------------------------------------------------------------------*
*&      Form  F_PROCESSA_DADOS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_processa_dados.

  DATA: lv_delta      TYPE mcwmit-ae_aq,
        lv_date_to    TYPE ltak-bdatu,
        lv_time_to    TYPE ltak-bzeit,
        lv_tabix      TYPE sy-tabix,
        lv_icon_tran  TYPE zde_icon,
        lv_data_ini   TYPE ltap-qdatu,
        lv_hora_ini   TYPE ltap-qzeit.

  FIELD-SYMBOLS: <fs_saida> TYPE ty_saida_2.

  "----------------------------------------------------------------------------------------------------------"
  "               Processa materiais que não possui movimentação de WM
  "----------------------------------------------------------------------------------------------------------"
  LOOP AT gt_mkpf INTO gs_mkpf.
    "Faz leitura de estoque....
    CLEAR gs_lqua.
    READ TABLE gt_lqua INTO gs_lqua WITH KEY mblnr = gs_mkpf-mblnr
                                             BINARY SEARCH.

    CHECK sy-subrc IS INITIAL.

    CLEAR gs_ltap.
    gs_ltap-lgnum = gs_lqua-lgnum.
    gs_ltap-matnr = gs_lqua-matnr.
    gs_ltap-charg = gs_lqua-charg.
    gs_ltap-nistm = gs_lqua-gesme.
    gs_ltap-meins = gs_lqua-meins.
    gs_ltap-nltyp = gs_lqua-lgtyp.
    gs_ltap-nlpla = gs_lqua-lgpla.
    gs_ltap-bdatu = gs_mkpf-budat.
    gs_ltap-bzeit = gs_mkpf-cputm.
    COLLECT gs_ltap INTO gt_ltap.
  ENDLOOP.

  "Ordena para leitura otimizada...
  SORT: gt_ltap    BY lgnum matnr charg ASCENDING tanum ASCENDING,
        gt_t334t   BY lgnum lgtkz,
        gt_mlgn    BY lgnum matnr,
        gt_makt    BY matnr,
        gt_zwmt143 BY lgnum ltkze,
        gt_lqua    BY lgnum matnr charg lgtyp lgpla,
        gt_t30c    BY lgnum lgbzo,
        gt_t301t   BY lgnum lgtyp.

  "----------------------------------------------------------------------------------------------------------"
  "               Processa materiais que estão fora do local de armazenamento
  "----------------------------------------------------------------------------------------------------------"
  "Remove Sistema Depósito/Material/Lote duplicados, para processamento dos dados...
  gt_ltap_aux[] = gt_ltap[].
  SORT gt_ltap_aux BY lgnum matnr charg.
  DELETE ADJACENT DUPLICATES FROM gt_ltap_aux COMPARING lgnum matnr charg.

  LOOP AT gt_ltap_aux INTO gs_ltap_aux.
    CLEAR: lv_tabix, lv_data_ini, lv_hora_ini.
    READ TABLE gt_ltap INTO gs_ltap WITH KEY lgnum = gs_ltap_aux-lgnum
                                             matnr = gs_ltap_aux-matnr
                                             charg = gs_ltap_aux-charg
                                             BINARY SEARCH.

    "Indice atual Sistema Depósito/Material/Lote...
    "Processa Todos as OTs do Depósito/Material/Lote...
    lv_tabix  = sy-tabix.
    lv_data_ini = gs_ltap-qdatu.
    lv_hora_ini = gs_ltap-qzeit.
    LOOP AT gt_ltap INTO gs_ltap FROM sy-tabix.
      CLEAR gs_saida.
      "Quebra por Sistema Depósito/Material/Lote..
      IF ( gs_ltap_aux-lgnum NE gs_ltap-lgnum )
        OR ( gs_ltap_aux-matnr NE gs_ltap-matnr )
          OR ( gs_ltap_aux-charg NE gs_ltap-charg ).
        EXIT.
      ENDIF.

      "Add 1 para leitura da OT anterior(Obs: gt_ltap[] esta ordenada com tanum do maior para o menor)
      ADD 1 TO lv_tabix.

      "Busca OT anterior a OT em processamento para Material/Lote...
      CLEAR: gs_ltap_before, lv_delta.
      READ TABLE gt_ltap INTO gs_ltap_before INDEX lv_tabix.

      "Se não encontrar mais OTs para o Material/Lote, então é a OT inicial...
      IF ( sy-subrc IS NOT INITIAL )
        OR ( gs_ltap_before-lgnum NE gs_ltap-lgnum )
          OR ( gs_ltap_before-matnr NE gs_ltap-matnr )
            OR ( gs_ltap_before-charg NE gs_ltap-charg ).
        gs_ltap_before-qdatu = sy-datum.
        gs_ltap_before-qzeit = sy-uzeit.
      ENDIF.

      gs_saida-data_ini = lv_data_ini.           "Data inicial que deve ser considerada no calculo...
      gs_saida-hora_ini = lv_hora_ini.           "Hora inicial que deve ser considerada no calculo...
      gs_saida-qdatu = gs_ltap-qdatu.            "Data de Entrada no Tipo de Depósito...
      gs_saida-qzeit = gs_ltap-qzeit.            "Hora de Entrada no Tipo de Depósito...
      gs_saida-bdatu = gs_ltap_before-qdatu.     "Data até(Data de Confirmação da OT corrente no Tipo de Depósito)...
      gs_saida-bzeit = gs_ltap_before-qzeit.     "Hora até(Data de Confirmação da OT corrente no Tipo de Depósito)...

      gs_saida-lgnum = p_lgnum.                  "Sistema de Depósito...
      gs_saida-werks = p_werks.                  "Centro...
      gs_saida-lgort = p_lgort.                  "Depósito...
      gs_saida-matnr = gs_ltap-matnr.            "Material...
      gs_saida-charg = gs_ltap-charg.            "Lote...
      gs_saida-nistm = gs_ltap-nistm.            "Quantidade...
      gs_saida-meins = gs_ltap-meins.            "Unidade de Medida...
      gs_saida-lgtyp = gs_ltap-nltyp.            "Tipo Deposito..
      gs_saida-lgpla = gs_ltap-nlpla.            "Posição..
      gs_saida-ltkze = gs_mlgn-ltkze.            "Código da categoria de depósito para entrada em depósito...
      gs_saida-tanum = gs_ltap-tanum.            "Numero da OT...
      COLLECT gs_saida INTO gt_saida.

      "Se a quantidade da próxima OT for difente da OT em processamento,
      "então altera data inicio considerada para o calculo...
      IF gs_ltap_before-nistm NE gs_ltap-nistm.
        lv_data_ini = gs_ltap_before-qdatu.
        lv_hora_ini = gs_ltap_before-qzeit.
      ENDIF.
    ENDLOOP.
  ENDLOOP.

  "----------------------------------------------------------------------------------------------------------"
  "               Processa materiais que ainda estão em conferência Recebimento
  "----------------------------------------------------------------------------------------------------------"
  "Monta range de status...
  PERFORM f_monta_range.

  LOOP AT gt_zwmt059 INTO gs_zwmt059.
    CLEAR gs_saida.
    "Faz leitura de SAD: zonas de preparação de material...
    CLEAR gs_t30c.
    READ TABLE gt_t30c INTO gs_t30c WITH KEY lgnum = p_lgnum
                                             lgbzo = gs_zwmt059-doca
                                             BINARY SEARCH.

    CLEAR: lv_date_to, lv_time_to.
    "Se conferencia não foi finalizada, então data atual, pois material esta no deposito de conferencia....
    IF gs_zwmt059-status NOT IN gr_status_fim_conf.
      gs_saida-bdatu = sy-datum.                   "Data até(Data atual pois o Material/Lote ainda esta no Tipo de Depósito em conferencia)...
      gs_saida-bzeit = sy-uzeit.                   "Hora até(Data atual pois o Material/Lote ainda esta no Tipo de Depósito em conferencia)...
    ELSE.
      "Se conferencia finalizada e data de armazenagem vazia, então data atual(Registros antigos)...
      IF ( gs_zwmt059-dt_armaz IS INITIAL )
        AND ( gs_zwmt059-hr_armaz IS INITIAL ).
        gs_saida-bdatu = gs_zwmt059-erdat_59.      "Data até(Data de criação do registro na tabela de item)...
        gs_saida-bzeit = gs_zwmt059-erzet_59.      "Hora até(Data de criação do registro na tabela de item)...
      ELSE.
        gs_saida-bdatu = gs_zwmt059-dt_armaz.      "Data até(Data de Armazenamento)...
        gs_saida-bzeit = gs_zwmt059-hr_armaz.      "Hora até(Data de Armazenamento)...
      ENDIF.
    ENDIF.

    "Se data de inicio da conferencia termolabil vazia, então data de criação do registro(Registros antigos)...
    IF ( gs_zwmt059-erdatterm IS INITIAL )
      AND ( gs_zwmt059_sum-erzetterm IS INITIAL ).
      gs_zwmt059-erdatterm = gs_zwmt059-erdat.
      gs_zwmt059-erzetterm = gs_zwmt059-erzet.
    ENDIF.

    gs_saida-lgnum      = p_lgnum.                 "Sistema de Depósito...
    gs_saida-werks      = p_werks.                 "Centro...
    gs_saida-lgort      = p_lgort.                 "Depósito...
    gs_saida-matnr      = gs_zwmt059-matnr.        "Material...
    gs_saida-charg      = gs_zwmt059-lichn.        "Lote...
    gs_saida-nistm      = gs_zwmt059-verme.        "Quantidade...
    gs_saida-meins      = gs_zwmt059-meins.        "Unidade de Medida...
    gs_saida-lgtyp      = gs_t30c-lgtor.           "Tipo Deposito..
    gs_saida-lgpla      = gs_t30c-lgbzo.           "Posição..
    gs_saida-qdatu      = gs_zwmt059-erdatterm.    "Data de Entrada no Tipo de Depósito...
    gs_saida-qzeit      = gs_zwmt059-erzetterm.    "Data de Saida do Tipo de Depósito...

    gs_saida-data_ini   = gs_zwmt059-erdatterm.    "Data de Entrada no Tipo de Depósito...
    gs_saida-hora_ini   = gs_zwmt059-erzetterm.    "Data de Saida do Tipo de Depósito...

    gs_saida-ltkze      = gs_mlgn-ltkze.           "Código da categoria de depósito para entrada em depósito...
    gs_saida-status_56  = gs_zwmt059-status.       "Status...
    gs_saida-ativid_56  = abap_true.
    COLLECT gs_saida INTO gt_saida.
  ENDLOOP.

  "----------------------------------------------------------------------------------------------------------"
  "               Processa todas as movimentações
  "----------------------------------------------------------------------------------------------------------"
  SORT: gt_ltap BY lgnum matnr charg nistm tanum.
  LOOP AT gt_saida ASSIGNING <fs_saida>.
    "Faz leitura de Textos breves de material...
    CLEAR gs_makt.
    READ TABLE gt_makt INTO gs_makt WITH KEY matnr = <fs_saida>-matnr
                                             BINARY SEARCH.

    <fs_saida>-maktx = gs_makt-maktx.   "Texto Bereve Material...

    "Faz leitura de Denominação da ctg.depósito...
    CLEAR gs_t301t.
    READ TABLE gt_t301t INTO gs_t301t WITH KEY lgnum = <fs_saida>-lgnum
                                               lgtyp = <fs_saida>-lgtyp
                                               BINARY SEARCH.

    <fs_saida>-ltypt = gs_t301t-ltypt.  "Tipo Depósito Texto..


    "Faz leitura de Dados de material por sistema de depósito...
    CLEAR gs_mlgn.
    READ TABLE gt_mlgn INTO gs_mlgn WITH KEY lgnum = <fs_saida>-lgnum
                                             matnr = <fs_saida>-matnr
                                             BINARY SEARCH.

    "Tempo de Armazenamento - Tipo de Depósito
    CLEAR gs_zwmt143.
    READ TABLE gt_zwmt143 INTO gs_zwmt143 WITH KEY lgnum = gs_mlgn-lgnum
                                                   ltkze = gs_mlgn-ltkze
                                                   BINARY SEARCH.


    "Verifica se o Material/Lote ainda esta em transito...
    CLEAR gs_lqua.
    READ TABLE gt_lqua INTO gs_lqua WITH KEY lgnum = <fs_saida>-lgnum
                                             matnr = <fs_saida>-matnr
                                             charg = <fs_saida>-charg
                                             lgtyp = <fs_saida>-lgtyp
                                             lgpla = <fs_saida>-lgpla
                                             BINARY SEARCH.

    IF ( sy-subrc IS INITIAL
      AND gs_lqua-btanr EQ <fs_saida>-tanum )
      OR ( <fs_saida>-ativid_56 EQ abap_true
       AND <fs_saida>-status_56 NOT IN gr_status_fim_conf ).
      <fs_saida>-icon_tran = icon_led_red.
    ELSE.
      <fs_saida>-icon_tran = icon_led_green.
    ENDIF.

    "Calcula diferença entra as datas...
    PERFORM f_calcula_diferenca USING <fs_saida>-data_ini
                                      <fs_saida>-bdatu
                                      <fs_saida>-hora_ini
                                      <fs_saida>-bzeit
                             CHANGING lv_delta.

    "Converte em HH:mm...
    PERFORM f_converte_minutos USING gs_zwmt143-timearm1
                       CHANGING <fs_saida>-time_1.

    "Converte em HH:mm...
    PERFORM f_converte_minutos USING gs_zwmt143-timearm2
                       CHANGING <fs_saida>-time_2.

    "Converte em HH:mm...
    PERFORM f_converte_minutos USING lv_delta
                       CHANGING  <fs_saida>-time_r.

    "Converte em HH:mm...
    PERFORM f_converte_minutos USING gs_zwmt143-timelimi
                       CHANGING  <fs_saida>-timelimi.


    "Verifica status para tempo 1...
    IF lv_delta > gs_zwmt143-timearm1.
      <fs_saida>-icon_time1 = icon_led_red.
    ELSE.
      <fs_saida>-icon_time1 = icon_led_green.
    ENDIF.

    "Verifica status para tempo 2...
    IF lv_delta > gs_zwmt143-timearm2.
      <fs_saida>-icon_time2 = icon_led_red.
    ELSE.
      <fs_saida>-icon_time2 = icon_led_green.
    ENDIF.

    <fs_saida>-umt = 'Hrs'.

  ENDLOOP.

ENDFORM.                    " F_PROCESSA_DADOS
*&---------------------------------------------------------------------*
*&      Form  F_MONTA_RANGE
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_monta_range.

  REFRESH: gr_status_fim_conf[].

  gr_status_fim_conf-sign = 'I'.
  gr_status_fim_conf-option = 'EQ'.
  gr_status_fim_conf-low = 'F'.     "Finalizado...
  APPEND gr_status_fim_conf.
  "
  gr_status_fim_conf-low = 'P'.     "Aguardando Armazenagem...
  APPEND gr_status_fim_conf.
  "
  gr_status_fim_conf-low = 'L'.     "Liberado para Amostragem...
  APPEND gr_status_fim_conf.
  "
  gr_status_fim_conf-low = 'R'.     "Liberado para Recontagem...
  APPEND gr_status_fim_conf.

ENDFORM.                    " F_MONTA_RANGE
*&---------------------------------------------------------------------*
*&      Form  F_CONVERTE_MINUTOS
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_GS_ZWMT143_TIMEARM1  text
*      <--P_GS_SUM_TIMEARM1  text
*----------------------------------------------------------------------*
FORM f_converte_minutos  USING    p_timearm1
                         CHANGING p_timearm1_c.

*  CALL FUNCTION 'CONVERSION_EXIT_SDURA_OUTPUT'
*    EXPORTING
*      input  = p_timearm1
*    IMPORTING
*      output = p_timearm1_c.

  DATA: lv_hours        TYPE i,
        lv_minutes_n(2) TYPE n.

  lv_hours     = p_timearm1 DIV  60.
  lv_minutes_n = p_timearm1 MOD 60.


  IF lv_hours < 9.
    WRITE lv_hours TO p_timearm1_c(5) NO-SIGN.
    WRITE '0' TO p_timearm1_c(4) NO-SIGN.
  ELSE.
    WRITE lv_hours TO p_timearm1_c(5) NO-SIGN.
  ENDIF.

  p_timearm1_c+5(1) = ':'.
  WRITE lv_minutes_n TO p_timearm1_c+6(2).
  CONDENSE p_timearm1_c NO-GAPS.

ENDFORM.                    " F_CONVERTE_MINUTOS
*&---------------------------------------------------------------------*
*&      Form  F_CALCULA_DIFERENCA
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*      -->P_GS_LTAP_FIRST_BDATU  text
*      -->P_SY_DATUM  text
*      -->P_GS_LTAP_FIRST_BZEIT  text
*      -->P_SY_UZEIT  text
*      <--P_LV_DELTA  text
*----------------------------------------------------------------------*
FORM f_calcula_diferenca USING p_date_from
                               p_date_to
                               p_time_from
                               p_time_to
                      CHANGING p_delta.

  "Calcula o tempo no Tipo de Depósito...
  CLEAR p_delta.
  CALL FUNCTION 'L_MC_TIME_DIFFERENCE'
    EXPORTING
      date_from       = p_date_from
      date_to         = p_date_to
      time_from       = p_time_from
      time_to         = p_time_to
    IMPORTING
      delta_time      = p_delta
    EXCEPTIONS
      from_greater_to = 1
      OTHERS          = 2.

ENDFORM.                    " F_CALCULA_DIFERENCA
*&---------------------------------------------------------------------*
*&      Form  F_VALIDA_AUTORIZACAO
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
*  -->  p1        text
*  <--  p2        text
*----------------------------------------------------------------------*
FORM f_valida_autorizacao .
  "Verifica autorização Centro e Depósito...
  AUTHORITY-CHECK OBJECT 'M_MSEG_LGO'
           ID 'ACTVT' FIELD '03'
           ID 'WERKS' FIELD p_werks
           ID 'LGORT' FIELD p_lgort.

  IF NOT sy-subrc IS INITIAL.
    "Falta autorização para Centro &, Depósito &!
    MESSAGE s245(zautowm) DISPLAY LIKE 'E'  WITH p_werks p_lgort.
    LEAVE LIST-PROCESSING.
  ENDIF.

  "Verifica autorização Nº Depósito...
  AUTHORITY-CHECK OBJECT 'L_LGNUM'
           ID 'LGNUM' FIELD p_lgnum
           ID 'LGTYP' DUMMY.

  IF sy-subrc IS NOT INITIAL.
    "Falta autorização para Sist. de Depósito &!
    MESSAGE s246(zautowm) DISPLAY LIKE 'E' WITH p_lgnum.
    LEAVE LIST-PROCESSING.
  ENDIF.
ENDFORM.                    " F_VALIDA_AUTORIZACAO

*----------------------------------------------------------------------*
***INCLUDE ZWMR174_O01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  STATUS_0200  OUTPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE status_0200 OUTPUT.
  SET PF-STATUS 'STATUS_0200'.
  SET TITLEBAR 'TITLE_0200'.

  "Montra fieldcat....
  PERFORM f_monta_fieldcat.

  "Verifica se container já foi carregado
  IF cl_alv_grid IS INITIAL.

    CREATE OBJECT cl_custom_container
      EXPORTING
        container_name              = 'CONTAINER_0200'
      EXCEPTIONS
        cntl_error                  = 1
        cntl_system_error           = 2
        create_error                = 3
        lifetime_error              = 4
        lifetime_dynpro_dynpro_link = 5.

    " Cria a instancia do ALV
    CREATE OBJECT cl_alv_grid
      EXPORTING
        i_parent = cl_custom_container.

    "Atualiza dados do layout para ALV
    CLEAR gs_layout.
    gs_layout-sel_mode     = 'A'.
    gs_layout-cwidth_opt   = abap_true.
    gs_layout-zebra        = abap_true.
    gs_layout-ctab_fname   = 'CELLCOLOR'.

    gs_sort-fieldname = 'MATNR'.
    gs_sort-down = 'X'.
    gs_sort-group = '*'.
    APPEND gs_sort TO gt_sort.

    gs_sort-fieldname = 'CHARG'.
    gs_sort-down = 'X'.
    gs_sort-group = '*'.
    APPEND gs_sort TO gt_sort.

    CLEAR gs_variant.
    gs_variant-report = 'ZWMR174'.

    "Exibe o relatório ALV (MONITOR)
    CALL METHOD cl_alv_grid->set_table_for_first_display
      EXPORTING
        i_save               = 'A'
        i_default            = abap_true
        is_layout            = gs_layout
        is_variant           = gs_variant
        it_toolbar_excluding = gt_ui_func
      CHANGING
        it_outtab            = gt_saida
        it_sort              = gt_sort
        it_fieldcatalog      = gt_fieldcat.

  ENDIF.

ENDMODULE.                 " STATUS_0200  OUTPUT

*----------------------------------------------------------------------*
***INCLUDE ZWMR174_I01 .
*----------------------------------------------------------------------*
*&---------------------------------------------------------------------*
*&      Module  USER_COMMAND_0200  INPUT
*&---------------------------------------------------------------------*
*       text
*----------------------------------------------------------------------*
MODULE user_command_0200 INPUT.
  CASE gv_ucomm.
    WHEN 'BACK' OR 'CANC'.
      SET SCREEN 0.
      LEAVE SCREEN.
    WHEN 'EXIT'.
      LEAVE PROGRAM.
  ENDCASE.
ENDMODULE.                 " USER_COMMAND_0200  INPUT